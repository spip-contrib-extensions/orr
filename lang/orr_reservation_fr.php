<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_orr_reservation' => 'Ajouter cette reservation',
	
	// D
	'date_de_debut' => 'Date de début',    
	'date_de_fin' => 'Date de fin',    	

	// E
	'erreur_reservation_date_fin_debut'=>'date de fin antérieur  ou égale à la date de début',	
	'erreur_reservation_date_occupe'=>'Vos dates de réservations ne sont pas libres !',
	'erreur_reservation_format_date'=>'Ce format de data n\'est pas reconnu.',	
	'explication_orr_date_debut' => 'Date de début de la réservation',
	'explication_orr_date_fin' => 'Date de fin de la réservation',
	'explication_orr_reservation_nom' => 'Nom de votre réservation (ex : réunion CA)',

	// I
	'icone_creer_orr_reservation' => 'Créer une reservation',
	'icone_modifier_orr_reservation' => 'Modifier cette reservation',
	'info_1_orr_reservation' => 'Une reservation',
	'info_aucun_orr_reservation' => 'Aucune reservation',
	'info_nb_orr_reservations' => '@nb@ reservations',
	'info_orr_reservations_auteur' => 'Les reservations de cet auteur',

	// L
	'label_orr_date_debut' => 'Date de début',
	'label_orr_date_fin' => 'Date de fin',
	'label_orr_reservation_nom' => 'Nom de la ressource',
	
    //N
    'nom_de_la_reservation'=>'Nom de la Réservation',

	// R
	'retirer_lien_orr_reservation' => 'Retirer cette reservation',
	'retirer_tous_liens_orr_reservations' => 'Retirer toutes les reservations',

	// T
	'texte_ajouter_orr_reservation' => 'Ajouter une reservation',
	'texte_changer_statut_orr_reservation' => 'Cette reservation est :',
	'texte_creer_associer_orr_reservation' => 'Créer et associer une reservation',
	'titre_langue_orr_reservation' => 'Langue de cette reservation',
	'titre_logo_orr_reservation' => 'Logo de cette reservation',
	'titre_orr_reservation' => 'Reservation',
	'titre_orr_reservations' => 'Reservations',
	'titre_orr_reservations_rubrique' => 'Reservations de la rubrique',
);

?>